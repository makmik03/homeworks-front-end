let name = prompt(`Hello! Write your name.`);
let age = +prompt(`Write your age.`);
let attempt = (`Welcome, ${name}.`);
let cancel = (`You are not allowed to visit this website`);

if (age > 0 ) {
    if(age < 18) {
        alert(cancel);
    } else if(age >= 18 && age < 22) {
        let accept = confirm(`Are you sure you want to continue?`);
        if (accept === true) {
            alert(attempt);
        } else {
            alert(cancel);
        }
    } else {
        alert(attempt);
    }
}
